<?php
require '../DB/DBAPI.php';
$ShftNum = $_GET["ShftNum"];
$status = "Cancelled";
$CancelRsn = $_GET["Rzn"];

$Cancelled = CancellShift($status, $CancelRsn, $ShftNum);

if($Cancelled["status"]=="ok")
{
    $rslt["msg"] = "Shift cancelled successfully. Wait as the system reloads.";
    $rslt["status"] = "ok";
}
else{
    $rslt["msg"] = "Shift cancellation failed. ERROR: ".$Cancelled["status"];
    $rslt["status"] = "error";
}

echo json_encode($rslt);

