USE [master]
GO
/****** Object:  Database [AxiRevColl]    Script Date: 1/23/2020 5:35:21 AM ******/
CREATE DATABASE [AxiRevColl]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AxiRevColl', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\AxiRevColl.mdf' , SIZE = 16384KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AxiRevColl_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\AxiRevColl_log.ldf' , SIZE = 3456KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AxiRevColl] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AxiRevColl].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AxiRevColl] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AxiRevColl] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AxiRevColl] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AxiRevColl] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AxiRevColl] SET ARITHABORT OFF 
GO
ALTER DATABASE [AxiRevColl] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AxiRevColl] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AxiRevColl] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AxiRevColl] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AxiRevColl] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AxiRevColl] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AxiRevColl] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AxiRevColl] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AxiRevColl] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AxiRevColl] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AxiRevColl] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AxiRevColl] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AxiRevColl] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AxiRevColl] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AxiRevColl] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AxiRevColl] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AxiRevColl] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AxiRevColl] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AxiRevColl] SET  MULTI_USER 
GO
ALTER DATABASE [AxiRevColl] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AxiRevColl] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AxiRevColl] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AxiRevColl] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [AxiRevColl] SET DELAYED_DURABILITY = DISABLED 
GO
USE [AxiRevColl]
GO
/****** Object:  Table [dbo].[luMeterHeader]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[luMeterHeader](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MeterNo] [nvarchar](50) NULL,
	[RouteNo] [nvarchar](50) NULL,
	[RouteID] [int] NULL,
	[RouteName] [nvarchar](50) NULL,
	[Acc] [nvarchar](50) NULL,
	[Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Ward] [nvarchar](50) NULL,
	[Suburb] [nvarchar](50) NULL,
	[lastreaddate] [nvarchar](50) NULL,
	[LastReading] [decimal](18, 3) NULL,
	[LastSyncDate] [datetime] NULL,
 CONSTRAINT [PK_luMeterHeader] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[luProductCategories]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[luProductCategories](
	[ProductCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_luProductCategories] PRIMARY KEY CLUSTERED 
(
	[ProductCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblBillables]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBillables](
	[PaymentID] [int] IDENTITY(1,1) NOT NULL,
	[ShiftNumber] [nvarchar](100) NULL,
	[ReferenceNumber] [nvarchar](50) NULL,
	[CustomerName] [nvarchar](100) NULL,
	[Amount] [decimal](18, 4) NULL,
	[PaymentMode] [nvarchar](50) NULL,
	[SalesRep] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[LoggedBy] [nvarchar](100) NULL,
	[DateUsed] [datetime] NULL,
	[UsedBy] [nvarchar](100) NULL,
	[ReasonsPaidFor] [nvarchar](200) NULL,
	[Status] [nvarchar](50) NULL,
	[Latitude] [nvarchar](50) NULL,
	[longitude] [nvarchar](50) NULL,
	[BalanceUnused] [decimal](18, 4) NULL,
	[Class] [nvarchar](50) NULL CONSTRAINT [DF_tblBillables_Class]  DEFAULT ('Payment'),
	[SyncDate] [datetime] NULL,
	[SyncBy] [int] NULL,
 CONSTRAINT [PK_tblBillables] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCompanyDetails]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCompanyDetails](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NULL,
	[CompanyAddress] [nvarchar](100) NULL,
	[CompanyBPN] [nvarchar](50) NULL,
	[CompanyVATN] [nvarchar](50) NULL,
	[CompanyEmail] [nvarchar](50) NULL,
	[CompanyLogo] [nvarchar](200) NULL,
	[DateSet] [datetime] NULL,
	[SetBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[McNo] [int] NULL,
	[OpCode] [nchar](3) NULL,
 CONSTRAINT [PK_tblCompanyDetails] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCurrency](
	[ExchangeRateID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [nvarchar](50) NULL,
	[CurrencyCode] [nvarchar](50) NULL,
	[USDExchangeRate] [decimal](18, 6) NULL,
	[DateSet] [datetime] NULL,
	[SetBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[status] [nvarchar](50) NULL,
	[SourceReference] [char](1) NULL,
 CONSTRAINT [PK_tblCurrency] PRIMARY KEY CLUSTERED 
(
	[ExchangeRateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCurrencyUpdateLog]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrencyUpdateLog](
	[CurrencyUpdateID] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyID] [int] NULL,
	[ExchangeRate] [decimal](18, 6) NULL,
	[DateLogged] [datetime] NULL,
	[LoggedBy] [int] NULL,
	[note] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCurrencyUpdateLog] PRIMARY KEY CLUSTERED 
(
	[CurrencyUpdateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCustomers]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomers](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNumber] [nvarchar](50) NULL,
	[CustomerName] [nvarchar](255) NULL,
	[Addr] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[LastPayDate] [datetime] NULL,
	[LastPayAmnt] [decimal](18, 2) NULL CONSTRAINT [DF_tblCustomers_StatusID]  DEFAULT ((1)),
	[Balance] [decimal](18, 2) NULL,
	[GPSLat] [nvarchar](50) NULL,
	[GPSLon] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_tblCustomers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblDailyShifts]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDailyShifts](
	[DailyShiftID] [int] IDENTITY(1,1) NOT NULL,
	[ShiftNumber] [nvarchar](50) NULL,
	[DateSynced] [datetime] NULL,
	[DateOpened] [datetime] NULL,
	[DateClosed] [datetime] NULL,
	[DailyTarget] [int] NULL,
	[MarshalID] [int] NULL,
	[Marshal] [nvarchar](50) NULL,
	[DepatureTimeComments] [nvarchar](255) NULL,
	[AssignedArea] [nvarchar](50) NULL,
	[ShiftStatus] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_tblDailyRouteSheets_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CancellationReason] [nvarchar](255) NULL,
	[CancelledBy] [int] NULL,
	[CancelledDate] [datetime] NULL,
	[DepartureDate] [datetime] NULL,
	[ActualCashRemitted] [decimal](18, 2) NULL,
	[CashSettlementNotes] [nvarchar](500) NULL,
	[CashVariance] [decimal](18, 2) NULL,
	[ReceivingCashier] [int] NULL,
	[TotalSales] [decimal](18, 2) NULL,
	[TransactionsCashReceipts] [decimal](18, 2) NULL CONSTRAINT [DF_tblDailyShifts_CashSettlementReceipts]  DEFAULT ((0)),
	[TransactionsCashSales] [decimal](18, 2) NULL CONSTRAINT [DF_tblDailyShifts_CashSettlementCashSales]  DEFAULT ((0)),
	[PriceListID] [int] NULL,
	[ShiftType] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblDailyShifts] PRIMARY KEY CLUSTERED 
(
	[DailyShiftID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblIncCodeMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIncCodeMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[IncCode] [nvarchar](50) NULL,
	[IncDescription] [nvarchar](50) NULL,
	[IncVat] [nchar](10) NULL,
	[GLAlloc] [nvarchar](50) NULL,
	[DateSynced] [datetime] NULL,
	[SyncedBy] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblInvoiceBasicInfo]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInvoiceBasicInfo](
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceTotal] [decimal](18, 4) NULL,
	[TotalVAT] [decimal](18, 4) NULL,
	[SalesManName] [nvarchar](50) NULL,
	[ShiftRefence] [nvarchar](50) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[SyncStatus] [int] NULL,
	[InvoiceStatus] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[InvDeliveryStatus] [nvarchar](50) NULL,
	[OutstandingBalance] [decimal](18, 4) NULL,
	[Class] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[munrctCreated] [bit] NULL,
	[munrctctrCreated] [bit] NULL,
 CONSTRAINT [PK_tblInvoiceBasicInfo] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblInvoices]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInvoices](
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NULL,
	[InvoiceUnique] [nvarchar](50) NULL,
	[InvoiceDate] [datetime] NULL,
	[InvoiceStatus] [nvarchar](50) NULL,
	[InvoiceTotal] [decimal](18, 2) NULL,
	[InvoiceSyncStatus] [int] NULL,
	[TotalVAT] [decimal](18, 2) NULL,
	[SalesmanName] [nvarchar](50) NULL,
	[PaymentMethod] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[ProductName] [nvarchar](50) NULL,
	[ProductPrice] [decimal](18, 2) NULL,
	[ProductQty] [decimal](18, 2) NULL,
	[ProductTotal] [decimal](18, 2) NULL,
	[ProductTaxCode] [nvarchar](5) NULL,
	[ProductVAT] [decimal](18, 2) NULL,
	[ProductDiscAmnt] [decimal](18, 2) NULL,
	[InvoiceType] [nvarchar](50) NULL,
	[TenderType] [nvarchar](50) NULL,
	[TenderAmount] [decimal](18, 2) NULL,
	[VarianceAmount] [decimal](18, 2) NULL,
	[GPSLatitude] [nvarchar](50) NULL,
	[GPSLongitude] [nvarchar](50) NULL,
	[ErpSync] [int] NULL,
	[TimeIn] [datetime] NOT NULL,
	[TimeOut] [datetime] NULL,
	[Area] [nvarchar](50) NULL,
	[SyncDate] [datetime] NULL,
	[SyncBy] [int] NULL,
	[munrctCreated] [bit] NULL,
	[munrctctrCreated] [bit] NULL,
 CONSTRAINT [PK_tblInvoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblInvoiceTender]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInvoiceTender](
	[InvoiceTenderID] [int] IDENTITY(1,1) NOT NULL,
	[RouteSheetNumber] [nvarchar](50) NULL,
	[InvNumber] [nvarchar](50) NULL,
	[Currency] [nvarchar](50) NULL,
	[AmountTendered] [decimal](18, 2) NULL,
	[BaseCurrencyValue] [decimal](18, 2) NULL,
	[DateSet] [datetime] NULL,
	[SetBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblInvoiceTender] PRIMARY KEY CLUSTERED 
(
	[InvoiceTenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblMeterReadingMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMeterReadingMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AccNum] [nvarchar](50) NULL,
	[MeterNum] [nvarchar](50) NULL,
	[RouteNo] [nvarchar](50) NULL,
	[Reading] [nvarchar](50) NULL,
	[ReadingDate] [datetime] NULL,
	[ReadBy] [int] NULL,
	[Latitude] [nvarchar](50) NULL,
	[Longitude] [nvarchar](50) NULL,
	[ImagePath] [nvarchar](150) NULL,
	[Description] [nvarchar](50) NULL,
	[Note] [nvarchar](50) NULL,
	[NoteID] [int] NULL,
 CONSTRAINT [PK_tblMeterReadingMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblMunrctctr]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMunrctctr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[McNo] [int] NULL,
	[Cash] [decimal](18, 4) NULL,
	[RecStatus] [nchar](10) NULL,
	[RecDate] [nchar](10) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_tblMunrctctr] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblNotesMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNotesMaster](
	[notesID] [int] IDENTITY(1,1) NOT NULL,
	[NoteCode] [nvarchar](50) NULL,
	[NoteDesc] [nvarchar](80) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_tblNotesMaster] PRIMARY KEY CLUSTERED 
(
	[notesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TblParkingSlots]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblParkingSlots](
	[ParkingAreaId] [int] IDENTITY(1,1) NOT NULL,
	[PointA] [nvarchar](100) NULL,
	[PointB] [nvarchar](100) NULL,
	[PointAlong] [nvarchar](100) NULL,
	[AreaLabel] [nvarchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_TblParkingSlots] PRIMARY KEY CLUSTERED 
(
	[ParkingAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblProducts]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProducts](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ParentCode] [nchar](10) NULL,
	[Code] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[UOM] [nvarchar](50) NULL,
	[ExclUnitPrice] [decimal](18, 2) NULL,
	[UnitPrice] [decimal](18, 2) NULL,
	[TaxCode] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_tblProducts_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[Deleted] [bit] NULL CONSTRAINT [DF_tblProducts_Deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_tblProducts] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblRecControl]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRecControl](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[McNo] [int] NULL,
	[OpCode] [nchar](3) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_tblRecControl] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblRecievePayments]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRecievePayments](
	[PaymentID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](100) NULL,
	[Amount] [decimal](18, 4) NULL,
	[PaymentMode] [nvarchar](50) NULL,
	[SalesRep] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[LoggedBy] [nvarchar](100) NULL,
	[DateUsed] [datetime] NULL,
	[UsedBy] [nvarchar](100) NULL,
	[ReasonsPaidFor] [nvarchar](200) NULL,
	[Status] [nvarchar](50) NULL,
	[Latitude] [nvarchar](50) NULL,
	[longitude] [nvarchar](50) NULL,
	[BalanceUnused] [decimal](18, 4) NULL,
	[Class] [nvarchar](50) NULL,
	[SyncDate] [datetime] NULL,
	[SyncBy] [int] NULL,
	[ReferenceNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblRecievePayments] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblRouteMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRouteMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RouteNo] [int] NULL,
	[RouteID] [int] NULL,
	[RouteDesc] [nvarchar](150) NULL,
	[NumOfMetres] [int] NULL,
	[LastReadDate] [date] NULL,
	[NextReadDate] [date] NULL,
	[SyncBy] [int] NULL,
	[SyncDate] [datetime] NULL,
 CONSTRAINT [PK_tblRouteMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUsers](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](15) NOT NULL,
	[Password] [varchar](500) NOT NULL,
	[UserFirstName] [nvarchar](50) NULL,
	[UserSurname] [nvarchar](50) NULL,
	[RouteName] [varchar](50) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](50) NULL,
	[UserType] [nvarchar](50) NULL CONSTRAINT [DF_tblUsers_UserType]  DEFAULT (N'SystemUser'),
	[UserPhoneNumber] [nvarchar](50) NULL,
	[MarshalServices] [nvarchar](100) NULL,
	[Deleted] [bit] NULL CONSTRAINT [DF_tblUsers_Deleted]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_tblUsers] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vwModifiedPayOuts]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwModifiedPayOuts]
AS
SELECT        dbo.tblInvoiceTender.*, dbo.tblInvoiceBasicInfo.InvoiceTotal, dbo.tblInvoiceBasicInfo.InvoiceNum
FROM            dbo.tblInvoiceTender INNER JOIN
                         dbo.tblInvoiceBasicInfo ON dbo.tblInvoiceTender.SetBy = dbo.tblInvoiceBasicInfo.SalesManName AND 
                         dbo.tblInvoiceTender.DateSet = dbo.tblInvoiceBasicInfo.CreatedDate

GO
/****** Object:  View [dbo].[vwPaymentsCashUp]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwPaymentsCashUp]
AS
SELECT        dbo.tblInvoiceTender.InvoiceTenderID, dbo.tblInvoiceTender.RouteSheetNumber, dbo.tblInvoiceTender.InvNumber, dbo.tblInvoiceTender.Currency, 
                         dbo.tblInvoiceTender.AmountTendered, dbo.tblInvoiceTender.BaseCurrencyValue, dbo.tblInvoiceTender.DateSet, dbo.tblInvoiceTender.SetBy, 
                         dbo.tblRecievePayments.SalesRep, dbo.tblRecievePayments.LoggedBy, dbo.tblRecievePayments.Amount
FROM            dbo.tblInvoiceTender INNER JOIN
                         dbo.tblRecievePayments ON dbo.tblInvoiceTender.DateSet = dbo.tblRecievePayments.CreatedDate

GO
/****** Object:  View [dbo].[vwFinalCashUp]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwFinalCashUp]
AS
  select sum(InvoiceTotal) as TotalCashUp, Currency, RouteSheetNumber from vwModifiedPayOuts where  Currency <> 'Account'
  
   group by Currency, RouteSheetNumber

   Union 

   select sum(Amount)/2  as TotalCashUp, Currency, RouteSheetNumber from vwPaymentsCashUp where  Currency <> 'Account' 
  
   group by Currency , RouteSheetNumber

GO
/****** Object:  View [dbo].[TblViewOwings]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TblViewOwings]
AS
SELECT     dbo.tblInvoiceBasicInfo.CustomerName, dbo.tblInvoiceBasicInfo.InvoiceStatus, dbo.tblInvoiceBasicInfo.InvoiceNum, dbo.tblInvoices.TimeIn, dbo.tblInvoices.TimeOut, dbo.tblInvoices.Area, 
                      dbo.tblInvoiceBasicInfo.OutstandingBalance AS InvoiceTotal
FROM         dbo.tblInvoiceBasicInfo INNER JOIN
                      dbo.tblInvoices ON dbo.tblInvoiceBasicInfo.InvoiceNum = dbo.tblInvoices.InvoiceNum
WHERE     (dbo.tblInvoiceBasicInfo.InvoiceStatus = 'Owing')





GO
/****** Object:  View [dbo].[vw_all_voids]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_all_voids]
AS
SELECT        PaymentID, ShiftNumber, ReferenceNumber, RIGHT(ReferenceNumber, LEN(ReferenceNumber) - 1) AS OriginalRefNumber, CustomerName, Amount, PaymentMode, 
                         SalesRep, CreatedDate, LoggedBy, DateUsed, UsedBy, ReasonsPaidFor, Status, Latitude, longitude, BalanceUnused, Class, SyncDate, SyncBy
FROM            dbo.tblBillables
WHERE        (Status = 'Void') AND (Amount < 0)

GO
/****** Object:  View [dbo].[vw_select_grouped_rec_controls]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_select_grouped_rec_controls]
AS
SELECT DISTINCT (CAST(CreatedDate AS DATE)) AS dates, SalesRep, ROW_NUMBER() OVER (PARTITION BY CreatedDate
ORDER BY CAST(CreatedDate AS DATE)) val
FROM            tblBillables
WHERE        ReasonsPaidFor = 'Billing' AND Status = 'Unused' 

GO
/****** Object:  View [dbo].[vw_tender_currency_analysis]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_tender_currency_analysis]
AS
SELECT        dbo.tblInvoiceTender.*, dbo.tblCurrency.SourceReference
FROM            dbo.tblCurrency INNER JOIN
                         dbo.tblInvoiceTender ON dbo.tblCurrency.Currency = dbo.tblInvoiceTender.Currency

GO
/****** Object:  View [dbo].[vwCustomerMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwCustomerMaster]
AS
SELECT        dbo.luMeterHeader.MeterNo, dbo.luMeterHeader.RouteNo, dbo.luMeterHeader.RouteID, dbo.luMeterHeader.RouteName, dbo.luMeterHeader.Acc, 
                         dbo.luMeterHeader.Address, dbo.luMeterHeader.Ward, dbo.luMeterHeader.Suburb, dbo.luMeterHeader.lastreaddate, dbo.luMeterHeader.LastReading, 
                         dbo.luMeterHeader.LastSyncDate, dbo.tblCustomers.CustomerName
FROM            dbo.luMeterHeader INNER JOIN
                         dbo.tblCustomers ON dbo.luMeterHeader.Acc = dbo.tblCustomers.CustomerNumber


GO
/****** Object:  View [dbo].[vwCustomerMeterReadingMaster]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwCustomerMeterReadingMaster]
AS
SELECT        dbo.tblMeterReadingMaster.AccNum, dbo.tblMeterReadingMaster.MeterNum, dbo.tblMeterReadingMaster.Reading, dbo.tblMeterReadingMaster.ReadingDate, 
                         dbo.tblMeterReadingMaster.Latitude, dbo.tblMeterReadingMaster.Longitude, dbo.tblMeterReadingMaster.Note, dbo.tblCustomers.CustomerName, 
                         dbo.tblCustomers.Addr, dbo.tblCustomers.Balance, dbo.tblMeterReadingMaster.ReadBy, dbo.tblMeterReadingMaster.ImagePath
FROM            dbo.tblMeterReadingMaster INNER JOIN
                         dbo.tblCustomers ON dbo.tblMeterReadingMaster.AccNum = dbo.tblCustomers.CustomerNumber


GO
/****** Object:  View [dbo].[vwIncToServices]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwIncToServices]
AS
SELECT        dbo.tblIncCodeMaster.IncCode, dbo.tblIncCodeMaster.GLAlloc, dbo.tblProducts.Code, dbo.tblProducts.Description
FROM            dbo.tblIncCodeMaster INNER JOIN
                         dbo.tblProducts ON dbo.tblIncCodeMaster.IncCode = dbo.tblProducts.ParentCode




GO
/****** Object:  View [dbo].[vwReceiptsToPost]    Script Date: 1/23/2020 5:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwReceiptsToPost]
AS
SELECT        dbo.tblProducts.ParentCode, dbo.tblProducts.Code, dbo.tblProducts.Description, dbo.tblInvoices.InvoiceTotal, dbo.tblInvoices.PaymentMethod, 
                         dbo.tblInvoices.CreatedDate, CAST(dbo.tblInvoices.CreatedDate AS DATE) AS DateSet, dbo.tblInvoices.InvoiceNum, dbo.tblInvoices.InvoiceUnique, 
                         dbo.tblInvoices.ProductTotal
FROM            dbo.tblInvoices INNER JOIN
                         dbo.tblProducts ON LEFT(dbo.tblInvoices.ProductName, CHARINDEX('#', dbo.tblInvoices.ProductName) - 1) = dbo.tblProducts.Code
WHERE        (dbo.tblInvoices.munrctCreated IS NULL)

GO
ALTER TABLE [dbo].[tblInvoiceBasicInfo] ADD  CONSTRAINT [DF_tblInvoiceBasicInfo_Class]  DEFAULT ('Invoice') FOR [Class]
GO
ALTER TABLE [dbo].[tblInvoices] ADD  CONSTRAINT [DF_tblInvoices_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tblInvoices] ADD  CONSTRAINT [DF_tblInvoices_ErpSync]  DEFAULT ((0)) FOR [ErpSync]
GO
ALTER TABLE [dbo].[tblRecievePayments] ADD  CONSTRAINT [DF_tblRecievePayments_Class]  DEFAULT ('Payment') FOR [Class]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblBillables"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 21
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1740
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_all_voids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_all_voids'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_select_grouped_rec_controls'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_select_grouped_rec_controls'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblCurrency"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 222
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "tblInvoiceTender"
            Begin Extent = 
               Top = 6
               Left = 260
               Bottom = 135
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_tender_currency_analysis'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_tender_currency_analysis'
GO
USE [master]
GO
ALTER DATABASE [AxiRevColl] SET  READ_WRITE 
GO
