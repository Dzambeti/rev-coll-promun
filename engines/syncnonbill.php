<?php

require '../DB/DBAPI.php';
require '../DB/ODBCAPI.php';




//global variables 

$ComInfo = this_co_details();

@$opcode = $ComInfo[0]["OpCode"];
@$mcno = $ComInfo[0]["McNo"];

if(empty($opcode) || empty($mcno)){
$rslt["status"] = "fail";
$rslt["msg"]  = "Please update machine number and operator code";
echo json_encode($rslt);
die();
}

$ref = 0;
$paytype = "C";
$seqno = 1;
$cash = 99999.00;
$recstatus = "U";




$State = $_GET["state"];
if ($State == "one") {
    $TransID = $_GET["recid"];
    $AccDetails = GetReceiptDetails($TransID);
    $mcno = 5;
    $cash = 99999.00;
    $recstatus = "U";
    $recdate = date("Y-m-d");

    $code = "ZZ";
    $acc = $AccDetails[0]["AccountNumber"];
    $amount = $AccDetails[0]["AmountPaid"];
    $recno = 1; // this increments as we add to 
    $opcode = 1;
    $ref = 0;
    $paytype = "C";
    $seqno = 1;
    $recname = $TransID;



    $CreateRec = CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname);
//create as many receipts as you want but with single ,machone number
    $CreateRecContrl = CreateMunrctctr($mcno, $cash, $recstatus, $recdate);

    if (($CreateRec["status"] == "ok") && ($CreateRecContrl["status"] == "ok")) {

        $MunRctSyncStatus = $CreateRec["status"];
        $MunRctCtrSyncStatus = $CreateRecContrl["status"];
        UpdateSyncStatus($MunRctSyncStatus, $MunRctCtrSyncStatus, $TransID);

        $rslt["msg"] = "Rec created successfully in promun.";
        $rslt["status"] = "ok";
    } else {

        $MunRctSyncStatus = $CreateRec["status"];
        $MunRctCtrSyncStatus = $CreateRecContrl["status"];
        UpdateSyncStatus($MunRctSyncStatus, $MunRctCtrSyncStatus, $TransID);

        $rslt["msg"] = "Failed to create a receipt. rec error " . $CreateRec["status"] . " and control error: " . $CreateRecContrl["status"];
        $rslt["status"] = "fail";
    }
    echo json_encode($rslt);


} else if ($State == "two") {
    //getReceiptsToSync

    $distinctDates = showDistincDate();
    if(empty($distinctDates)){
        $rslt["status"] = "fail"; 
        $rslt["msg"] = "No transaction available to sync.";
    }

    else{
        
        foreach($distinctDates as $date){
            $distDate = $date["dates"];
            $i = 1;
            // show all date transactions
            $transactions = getDateReceipts($distDate);
            // print_r($transactions);
            // die();
            foreach($transactions as $rec){
                $Prod = $rec["Code"];
              //  $arr = explode("#", $Prod, 2);
                $Pcode = $Prod;
                $code = trim($rec["ParentCode"]);
               $SvcMaster = getIncCode($code);
            
                
                $acc = $SvcMaster[0]["GLAlloc"];
               // $ref = $rec["InvoiceNum"];
                $paytype = "B";
                $ref = 0;
      
                $amount = $rec["ProductTotal"];
                $recno = $i; 
                $recname = $rec["InvoiceNum"];

                $seqNo = 1;
                $finalRecDate = date("Y-m-d",strtotime($distDate));
          
                // echo "Code :$code, Acc: $acc, Amount: $amount, MCNO: $mcno, Rec-date: $finalRecDate , Rec:No $recno, OP-Code: $opcode,
                // Reference:  $ref, Paytype: $paytype,SeqNo:  $seqNo, Rec-Status: $recstatus, Rec-Name: $recname";

                // die();

                $CreateRec = CreateMunrct($code, $acc, $amount, $mcno, $finalRecDate , $recno, $opcode, $ref, $paytype, $seqNo, $recstatus, $recname);

                if ($CreateRec["status"] == "ok") {
                    $MunRctSyncStatus = $CreateRec["status"];
                    if($MunRctSyncStatus == "ok"){
                        $syncstatus = 1;
                    }else{
                        $syncstatus = NULL;
                    }
                     UpdateRecSyncStatus($syncstatus,$recname);

                            $i ++ ;
                    if($i == sizeof($transactions)){
                        //create munrctctr
                        
                $cash = 99999.00;
                $recstatus = "U";
            
           $CreatHdr =  CreateMunrctctr($mcno, $cash, $recstatus, $finalRecDate);
           if($CreatHdr["status"] == "ok"){
            updateMunrctctrStatus($syncstatus,$distDate);
           }
            }

                    @$rslt["msg"] = "Receipts posted successfully in promun. With header status: ".$CreatHdr["status"];
                    $rslt["status"] = "ok";
                }  else {
                $rslt["msg"] = "Failed to create receipting records. Error: " . $CreateRec["status"];
                $rslt["status"] = "fail";
            }

            
            }

        }

    }



   
    echo json_encode($rslt);
}





