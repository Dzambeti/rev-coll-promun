<?php
require '../DB/DBAPI.php';


@$currency_name = $_POST['currency_name'];
@$currency_ex_rate = $_POST['ex_rate'];
@$curr_code = $_POST['currency_code'];
@$currency_type = $_POST["pay_type"];

$GetCurr = GetCurrency($curr_code);



if($currency_ex_rate=="" || $currency_name=="" || $currency_type == ""){
     $rslt["msg"] = 'Currency name or Exchange rate or currency type can not be empty. Please fill all fields!';
	 $rslt["status"] = "fail";

}
else if(sizeof($GetCurr)>0){
	$rslt["msg"] = 'Currency Already exist!';
	 $rslt["status"] = "fail";
}
else{
    $new_curr = create_currency_exchange_rates($currency_name,$curr_code,$currency_ex_rate,$currency_type);
    if($new_curr['status']=="ok"){
           
		$rslt["status"] = "ok";
		
        $rslt["msg"] = 'Currency added successfully, please wait as the system effects changes!';
    }
    else{
        $rslt["msg"] = 'Failed to add currency. ERROR: '.$new_curr['status'];
		$rslt["status"] = "fail";
    }
}

echo json_encode($rslt);

         