<?php
require_once 'DB/DBAPI.php';

if($_SESSION['acc']!="")
{
	redirect("index?acc=".$_SESSION['acc']);
}
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
	logout();
	redirect("login");
}
if(!isset($_SESSION['acc']))
{
    redirect("login");
}
