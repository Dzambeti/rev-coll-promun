<?php

require '../DB/DBAPI.php';
$PaymentID = $_GET["id"];
$Username = $_SESSION["Username"];

$PaymentDet = GetPaymentWithId($PaymentID);
$CarReg = $PaymentDet[0]["CustomerName"];

$Amnt = $_GET["amount"];
$CarDet = GetOutstandingSales($CarReg);

//get allrec eipt by date asc
foreach ($CarDet as $Data) {
    $RecNum = $Data["InvoiceNum"];
    $Bal = $Data["InvoiceTotal"];
    if ($Bal < $Amnt) {
        //update bal = 0 and subtract payment unused
        $Amnt = $Amnt-$Bal;
        $Bal = 0;
        $InvStatus = "Paid";
        $PaymentStatus = "Unused";
       
    }
    else if ($Bal == $Amnt){
        $Amnt = 0;
        $Bal = 0;
        $InvStatus = "Paid";
        $PaymentStatus = "Used";
    }
    else if($Bal>$Amnt){
        $Bal = $Bal-$Amnt;
        $Amnt = 0;
        $InvStatus = "Owing";
        $PaymentStatus = "Used";
    }
    
  $UpdateInvStatus =   UpdateInvoiceStatus($Bal,$InvStatus, $RecNum);
  $UpdatePaymentStatus = UpdatePayment($Amnt,$PaymentStatus,$Username,$PaymentID);
}

if($UpdateInvStatus["status"]=="ok" && $UpdatePaymentStatus["status"]=="ok")
{
    $rslt["msg"] = "Payment for $CarReg has been Updated Successfully!!";
    $rslt["status"] = "ok";
}
else{
    $rslt["msg"] = "Update Failed. Invoice update error - ".$UpdateInvStatus["status"]." and Update payments status error - ".$UpdatePaymentStatus["status"];
}

echo json_encode($rslt);
    