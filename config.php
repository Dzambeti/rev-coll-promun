<?php

require 'engines/UdpdateNullInv.php';
$UnusedPayments = sizeof(GetMenuPayments());
 
 ?>



<link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


<div class="loader-bg"></div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-teal lighten-1">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
<div class="mn-content fixed-sidebar">

    <header class="mn-header navbar-fixed">
        <nav class="cyan darken-1">
            <div class="nav-wrapper row">
                <section class="material-design-hamburger navigation-toggle">
                    <a href="javascript:void(0)" data-activates="slide-out" class="button-collapse show-on-large material-design-hamburger__icon">
                        <span class="material-design-hamburger__layer"></span>
                    </a>
                </section>
                <div class="header-title col s5 m5 l5 ">      
                    <span class="chapter-title">Aximos - Revenue Collection</span>
                </div>

            </div>
        </nav>
    </header>



    <aside id="slide-out" class="side-nav white fixed">
        <div class="side-nav-wrapper">
            <div class="sidebar-profile">
                <div class="sidebar-profile-image">
                    <img src="assets/images/profile-image.png" class="circle" alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p><?php echo $UserType; ?></p>
                        <span><?php echo $_SESSION["Username"]; ?><i class="material-icons right">arrow_drop_down</i></span>
                    </a>
                </div>
            </div>
            <div class="sidebar-account-settings">


                <ul>

                    <li class="no-padding">
                        <a class="waves-effect waves-grey" href="logout?logout=true"><i class="material-icons">exit_to_app</i>Sign Out</a>
                    </li>
                </ul>
            </div>
            <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                
                <li class="no-padding active">
                    <a class="waves-effect waves-grey active" href="index"><i class="material-icons dp48">settings</i>Dashboard</a>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManageServices"><i class="material-icons">star_border</i>Services</a>

                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManageSlots"><i class="material-icons">theaters</i>Activity Slots</a>

                </li>
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManageShifts"><i class="material-icons">mode_edit</i>Shifts</a>

                </li>
             
                <li class="no-padding">
                     <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">credit_card</i>Tickets <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a> <div class="collapsible-body">
                        
                        <ul>
                             <li><a href="ManageReceipts?Shft=_all">All Tickets</a></li> 
                            <li><a href="ManageReceipts?Shft=_out">Outstanding Tickets</a></li>     
                            <li><a href="ManageReceipts?Shft=_in">Paid Up Tickets</a></li>  
                        </ul>                        
                         <ul>
                                                          
                        </ul>
                       
                    </div>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="Collections"><i class="material-icons">phonelink_ring</i>Collections (Accounts)</a>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="wi wi-barometer"></i>Water Meters<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        
                        <ul>
                             <li><a href="MeterReads">Routes</a></li> 
                            <li><a href="MeterReads">Reading Master</a></li>     
                            <li><a href="MeterDist">Meter Distribution</a></li>  
                        </ul>                        
                         <ul>
                                                          
                        </ul>
                       
                    </div>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManagePayments"><i class="material-icons">desktop_windows</i>Payments (<?php echo $UnusedPayments; ?>)</a>
                    </li>
                  <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManageCurrencies"><i class="material-icons">credit_card</i>Currencies</a>

                </li>
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">my_location</i>Maps <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="ReceiptDist?State=all">Receipt Distribution</a></li>                                
                        </ul>
                        
                         <ul>
                             <li><a href="CustDist">Rate Payers</a></li>                                
                        </ul>
                        
                        <ul>
                             <li><a href="ReceiptDist?State=all">Collections Distribution</a></li>                                
                        </ul>
                    </div>
                </li>
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey" href="ManageUsers"><i class="material-icons">grid_on</i>Users</a>

                </li>

                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">swap_vert</i>Master Data <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="ManageIncCodes">Income Codes</a></li>                                
                        </ul>
                        <ul>
                            <li><a href="ManageDebtors">Debtors Master-file</a></li>                                
                        </ul>
                        
                        <ul>
                            <li><a href="MeterMaster">Update Meter Data</a></li>                                
                        </ul>
                        
                         <ul>
                            <li><a href="RoutesMaster">Update Routes</a></li>                                
                        </ul>
                        
                        
                        <ul>
                            <li><a href="NotesMaster">Update Notes</a></li>                                
                        </ul>

                       
                    </div>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">swap_vert</i>Synchronization <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        
                        <ul>
                            <li><a href="SyncReceipts">Sync Non Billable (<?php echo count(showRecToSync()); ?>)</a></li>                                
                        </ul>
                        <ul>
                            <li><a href="SyncCollections">Sync Billable (<?php echo count(GetCollectionToSync()); ?>)</a></li>                                
                        </ul>
<!--                         
                        <ul>
                            <li><a href="EOD">End Of Day</a></li>                                
                        </ul> -->
                    </div>
                </li>
                
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">code</i>Exceptions <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="">Failed Sync (Non Billable)</a></li>                                
                        </ul>
                        <ul>
                            <li><a href="">Failed Sync (Billable)</a></li>                                
                        </ul>

                      
                    </div>
                </li>
                
                 <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">settings</i>Administration <i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="LaInfo"><i class="material-icons">grid_on</i>Company Details</a></li>                                
                        </ul>
                        <ul>
                            <li><a href="logout?logout=true"><i class="material-icons">exit_to_app</i>Logout</a></li>                                
                        </ul>

                      
                    </div>
                </li>
                
                
                

            </ul>
            <div class="footer">
                <p class="copyright">Axis Solutions Africa ©</p>
                <!--<a href="#!">Privacy</a> &amp; <a href="#!">Terms</a>-->
            </div>
        </div>
    </aside>

