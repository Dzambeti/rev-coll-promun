<?php

require '../DB/DBAPI.php';


$getPayments = GetMenuPayments();



$Username = $_SESSION["Username"];

foreach($getPayments as $payments)
    {

$PaymentID = $payments["PaymentID"];
$Amnt = $payments["Amount"];
$CarReg = $payments["CustomerName"];

$CarDet = GetOutstandingSales($CarReg);

//get allrec eipt by date asc
if(!empty($CarDet)){
foreach ($CarDet as $Data) {
    $RecNum = $Data["InvoiceNum"];
    $Bal = $Data["InvoiceTotal"];
    if ($Bal < $Amnt) {
        //update bal = 0 and subtract payment unused
        $Amnt = $Amnt-$Bal;
        $Bal = 0;
        $InvStatus = "Paid";
        $PaymentStatus = "Unused";
       
    }
    else if ($Bal == $Amnt){
        $Amnt = 0;
        $Bal = 0;
        $InvStatus = "Paid";
        $PaymentStatus = "Used";
    }
    else if($Bal>$Amnt){
        $Bal = $Bal-$Amnt;
        $Amnt = 0;
        $InvStatus = "Owing";
        $PaymentStatus = "Used";
    }
    
  $UpdateInvStatus =   UpdateInvoiceStatus($Bal,$InvStatus, $RecNum);
  $UpdatePaymentStatus = UpdatePayment($Amnt,$PaymentStatus,$Username,$PaymentID);
}

if($UpdateInvStatus["status"]=="ok" && $UpdatePaymentStatus["status"]=="ok")
{
    $rslt["msg"] = "Payments have been Updated Successfully!!";
    $rslt["status"] = "ok";
}
else{
    $rslt["msg"] = "Update Failed. Invoice update error - ".$UpdateInvStatus["status"]." and Update payments status error - ".$UpdatePaymentStatus["status"];
}
}
else{
    $rslt["msg"] = "No reference found for an intended payment.";
}
}
echo json_encode($rslt);
    