<?php
require '../DB/DBAPI.php';

$companyName = $_POST["CoName"];
$CompanyBPN = $_POST["BPN"];
$CompanyAddr = $_POST["Address"];
$companyVAT = $_POST["Vat"];
$CoEmail = $_POST["email"];
$opcode = $_POST["OpCode"];
$McNo = $_POST["McNO"];
$companyLogo = "";
//$CoDetails = this_co_details();
//$id = $CoDetails[0]["id"];

$UpdateData = edit_company_details($companyName, $CompanyAddr, $CompanyBPN, $companyVAT, $CoEmail, $companyLogo,$McNo, $opcode);

  if($UpdateData['status']=="ok"){
    $result['msg']='<div class="alert alert-success text-success text-green">Company Details Updated Successfully. </div>';
    $result['log']='pass';
  }else{
      $result['msg']='<div class="alert alert-danger>"Could not update details! ERROR: '.$UpdateData['status'].'</div>';
    $result['log']='fail';
  }


echo json_encode($result);