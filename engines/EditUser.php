<?php

require '../DB/DBAPI.php';

$UserID = $_GET["UID"];
$userfirstmame = $_POST['FirstName'];
$usersurname = $_POST['LasName'];
$username = $_POST['Username'];
$email = $_POST['email'];
$jobtitle = $_POST['JobTitle'];
$phone_number = $_POST['phone'];
$user_group = $_POST["usergroup"];
$AllServices = "All";
$UserInfo = UserDetails($UserID);

$user_rec_info = get_user_rec_control($UserID);

if(empty($user_rec_info)){
    $mc_no = NULL;
    $op_code = NULL;
        }
        else{
            $mc_no = $user_rec_info[0]["McNo"];
            $op_code =  $user_rec_info[0]["OpCode"]; 
        }

if ($user_group == "") {
    $user_group = $UserInfo[0]["UserType"];
    $MarshalServices = $UserInfo[0]["MarshalServices"];
   
}

 if ($user_group == "Marshal") {


    $mc_no = $_POST["mcno"];
    $op_code = $_POST["opcode"];
    if($mc_no == NULL || $op_code == NULL){
    $rslt["msg"] = 'Machine number and or operator code can not be null!';
    $rslt["status"] = "error";
    echo json_encode($rslt);
    die();
    }
    else if($mc_no > 999){
        $rslt["msg"] ='Machine number can not be greater 999.!'; 
        $rslt["status"] = "error";
        echo json_encode($rslt);
        die();
    }
    @$Services = $_POST["servicecategories"];

    if (empty($Services)) {
        $MarshalServices = $UserInfo[0]["MarshalServices"];
    } else {
        if (in_array($AllServices, $Services)) {
            $AllCodes = array();
            foreach (ShowAllProdCodes() as $Codes) {
                $Cod = $Codes["Codes"];
                array_push($AllCodes, $Cod);
            }
            $MarshalServices = implode($AllCodes, ",");
        } else {
            $MarshalServices = implode($Services, ",");
        }
    }
} else {
    // $mc_no = NULL;
    // $op_code = NULL;
    $MarshalServices = "";
}

//check email, phone number and  username
$usernames = array();
$emails = array();
$phones = array();

$all_users = get_all_users_expt_this($UserID);
foreach ($all_users as $user) {
    $DB_username = $user['Username'];
    $db_email = $user['EmailAddress'];
    $db_phoneNUmber = $user['UserPhoneNumber'];
    array_push($phones, $db_phoneNUmber);
    array_push($emails, $db_email);
    array_push($usernames, $DB_username);
}
if (empty($username) || empty($user_group)) {
    $rslt["msg"] = 'Username or user group can not be empty!';
    $rslt["status"] = "error";
} elseif (in_array($phone_number, $phones)) {
    $rslt["msg"] = 'Phone number already in use by another user. Verify this phone number please.';
    $rslt["status"] = "error";
} elseif (in_array($email, $emails)) {
    $rslt["msg"] = ' Email Address already in use. Enter correct email address';
    $rslt["status"] = "error";
} elseif (in_array($username, $usernames)) {
    $rslt["msg"] = ' Username in use. Please choose another username.';
    $rslt["status"] = "error";
} else {
         $edit_user =   edit_user($username, $userfirstmame, $usersurname, $jobtitle, $email, $user_group, $phone_number, $MarshalServices, $UserID);
    if ($edit_user['status'] == "ok") {
        if($user_group == "Marshal"){
            $update_rec = update_rec_control($mc_no,$op_code,$UserID);
        }
       
        $rslt["msg"] = 'User Profile has been edited successfully!';
        $rslt["status"] = "ok";
    } else {
        $rslt["status"] = "error";
        $rslt["msg"] = ' Failed to edit user account. ERROR: ' . $edit_user['status'];
    }
}

echo json_encode($rslt);







