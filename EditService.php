<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
    $ServiceID =  $_GET["sid"];
    $SvcInfo = ShowServiceData($ServiceID);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Aximos | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
         <link href="assets/plugins/select2/css/select2.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row">

                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Edit Service</span>
                                </div>

                            </div>

                            <br>
                             <div class="row">
                <form class="EditService col s12" method="post" >
                     <div class="row">
                        <div class="input-field col s6">

                            <select name="promuninccode" class="js-states browser-default" tabindex="-1" style="width: 100%" id="basic">
                                 <option value="">Parent Services</option>
                                <?php
                                $promunServices = GetIncCodes();
                                foreach ($promunServices as $Svc) {
                                    $IncCode = $Svc["IncCode"];
                                    $CodeDesc = $Svc["IncDescription"];
                                    ?>
                                    <option value="<?php echo $IncCode; ?>"> <?php echo $IncCode." - ".$CodeDesc; ?></option>
                                <?php } ?>
                               
                            </select>
                             
                           
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Service Code" value="<?php echo $SvcInfo[0]["Code"]; ?>"  id="code" name="code" type="text" class="validate">
                            <label for="CoName">Service Code</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Service Description" value="<?php echo $SvcInfo[0]["Description"]; ?>" id="desc" name="desc" type="text" class="validate">
                            <label for="LasName">Service Description</label>
                        </div>
                    </div>

                   <div class="row">
                        <div class="input-field col s6">
                            <select  name="uom" class="js-states browser-default" tabindex="-1" style="width: 100%">
                                 <option value="">Select Unit Of Measure</option>
                                <option value="hr">Per Hour</option>
                                <option value="day">Per Day</option>
                                <option value="wk">Per Week</option>
                                <option value="mt">Per Month</option>
                                <option value="yr">Per Annum</option>
                                <option value="of">Once Off</option>
                                <option value="load">Per Load</option>
                            </select>
                           
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <select id="taxcode" name="taxcode" class="js-states browser-default"  tabindex="-1" >
                                 <option value="">Choose Tax Code</option>
                                <option value="A"> A (15% VAT)</option>
                                <option value="A">B (0% VAT)</option>  
                            </select>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Unit Price" value="<?php echo $SvcInfo[0]["UnitPrice"]; ?>" id="unitprice" name="unitprice" type="number" class="validate">
                            <label for="unitprice">Unit Price</label>
                        </div>
                    </div>
                    
                    <div class ="row">

                <div class="col s8 l8 m8">
                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                    <a type="submit" name="BtnEditService" class= "BtnEditService waves-effect waves-light btn blue m-b-xs">Edit Service</a>
                </div>
                

            </div>


                </form>
            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>



    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
     <script src="assets/plugins/select2/js/select2.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>
    <script>
        $(document).ready(function () {
            
            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);
            
            $('select').select2();

            $(".BtnEditService").click(function (ev) {
                ev.preventDefault();
                var id = '<?php echo $ServiceID; ?>';
                $.post("engines/EditService.php?id="+id, $(".EditService").serialize(),
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });
            
           

        });
    </script>
</body>
</html>