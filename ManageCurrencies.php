<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis || <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
 <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row FocusDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>New Currency</h5>


                            <div class="row">
                                <div class="input-field col s4">
                                    <input id="cudesc" name="cudesc" type="text" class="validate ">
                                    <label for="cudesc">Currency Description</label>
                                </div>

                                <div class="input-field col s4">
                                    <input id="cucode" name="cucode" type="text" class="validate">
                                    <label for="cucode">Currency Code</label>
                                </div>

                                <div class="input-field col s4">
                                    <input id="exrate" name="exrate" type="number" class="validate">
                                    <label for="exrate">Exchange Rate</label>
                                </div>

                                <div class="input-field col s4">
                                    <select name="paytype" id="paytype" class="form-control">
                                   <option value=""> Select Pay Type </option>
                                    <option value="C">CASH</option>
                                    <option value="B">BANK Transfer</option>
                                    <option value="V">Swipe</option>
                                    <option value="T">Telecash</option>
                                    <option value="E">Ecocash</option>
                                    <option value="O">One Money</option>
                                    <option value="Q">Cheques</option>
                                    
                                    </select>
                                    <label for="exrate">Pay Type</label>
                                </div>
                            </div>



                            <div class ="row">

                                <div class="col s4 l4 m4">
                                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                    <a type="submit" name="BtnCreateCurrency" class= "BtnCreateCurrency waves-effect waves-light btn blue m-b-xs">Create Currency</a>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>

            <div class="row EditDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>Edit Currency</h5>


                            <div class="row">
                                <div class="input-field col s4">
                                    <input id="editcudesc" placeholder="" name="editcudesc" type="text" class="validate ">
                                    <label for="editcudesc">Currency Description</label>
                                </div>
                                   
                                
                                <div class="input-field col s4">
                                    <input id="editcucode" placeholder="" name="editcucode" type="text" class="validate">
                                    <label for="editcucode">Currency Code</label>
                                </div>

                                <div class="input-field col s4">
                                    <input id="editexrate" placeholder="" name="editexrate" type="number" class="validate">
                                    <label for="editexrate">Exchange Rate</label>
                                </div>
                            </div>



                            <div class ="row">

                                <div class="col s4 l4 m4">
                                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                    <a type="submit" name="BtnEditCurrency" class= "BtnEditCurrency waves-effect waves-light btn green m-b-xs">Update Currency</a>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">All Currencies</span>
                                </div>

                                <div class="col s6 m6 l6 right-align">
                                    <a class="btnFocusDiv waves-effect waves-light btn red m-b-xs">Create Currency</a>
                                </div>

                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Currency Code</th>
                                        <th>Description</th>
                                        <th>Pay Type</th>
                                        <th>USD Exchange Rate</th>
                                        <th>Date Created</th>
                                        <th>Last Update</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Currs = get_tender_types();
                                    foreach ($Currs as $cur) {
                                        $id = $cur["ExchangeRateID"];
                                        $Code = $cur["CurrencyCode"];
                                        $Desc = $cur["Currency"];
                                        $ExChRate = $cur["USDExchangeRate"];
                                        $currency_type = $cur["SourceReference"];
                                        if($currency_type == "C"){
                                            $type = "Cash";
                                        }
                                        else if($currency_type == "B"){
                                            $type = "Bank Transfer";
                                        }
                                        else if($currency_type == "V"){
                                            $type = "POS Transaction";
                                        }
                                        else if($currency_type == "T"){
                                            $type = "Telecash";
                                        }
                                        else if($currency_type == "E"){
                                            $type = "Ecocash";
                                        }
                                        else if($currency_type == "O"){
                                            $type = "One Money";
                                        }
                                        else if($currency_type == "Q"){
                                            $type = "Cheques";
                                        }
                                        $LastUpdate = date("d M y", strtotime($cur["UpdatedDate"]));
                                        $CreatedDate = date("d M y", strtotime($cur["DateSet"]));
                                        ?>
                                        <tr id='<?php echo $Code; ?>'>
                                            <td><?php echo $Code; ?> </td>
                                            <td><?php echo $Desc; ?> </td>                                            
                                            <td><?php echo $type; ?> </td>
                                            <td> <?php echo $ExChRate; ?></td>
                                            <td><?php echo $CreatedDate; ?></td>
                                            <td><?php echo $LastUpdate; ?></td>
                                    <input type="hidden" id="<?php echo $id; ?>" code="<?php echo $Code; ?>" desc="<?php echo $Desc; ?>" excrate="<?php echo $ExChRate; ?>"/> 
                                    <td>
                                        <a class="btn-floating btn-small waves-effect waves-light blue editCurr" title="Edit Currency"><i class="small material-icons">mode_edit</i></a>
                                    </td>

                                    </tr> 

                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>


    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>

    <script>
        $(document).ready(function () {



            $(".FocusDiv").hide();
            $(".EditDiv").hide();


            $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });
                var id;
            $('.editCurr').click(function () {
                var tr = $(this).closest('tr');
                var code = $(tr).find("input[type='hidden']").attr("code");
                var desc = $(tr).find("input[type='hidden']").attr("desc");
                var exrate = $(tr).find("input[type='hidden']").attr("excrate");
                id = $(tr).find("input[type='hidden']").attr("id");
                $("#editcucode").val(code);
                $("#editcudesc").val(desc);
                $("#editexrate").val(exrate);
               
                $(".EditDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });



            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);



            $(".BtnCreateCurrency").click(function (ev) {
                ev.preventDefault();
                // alert( $("#paytype").val());
                $.post("engines/CreateCurrency.php",
                        {
                            currency_name: $("#cudesc").val(),
                            ex_rate: $("#exrate").val(),
                            currency_code: $("#cucode").val(),
                            pay_type: $("#paytype").val()
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });
            
            $(".BtnEditCurrency").click(function(ev){
                ev.preventDefault();
                $.post("engines/EditCurrency.php",
                        {
                            currency_name: $("#editcudesc").val(),
                            ex_rate: $("#editexrate").val(),
                            currency_code: $("#editcucode").val(),
                            id:id
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
                });

        });
    </script>
</body>
</html>