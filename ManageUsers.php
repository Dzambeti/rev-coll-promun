<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row">

                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Users Master file</span>
                                </div>

                                <div class="col s6 m6 l6 right-align">
                                    <a class="modal-trigger waves-effect waves-light btn red m-b-xs" href="#modal1">Add User</a>
                                </div>

                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Position</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>User group</th>
                                        <th>Created date</th>
                                        <th>Created By</th>
                                         <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Users = get_all_users();
                                    foreach ($Users as $user) {
                                        $user_id = $user['UserID'];
                                        $username = $user['Username'];
                                        $email = $user['EmailAddress'];
                                        $phone = $user['UserPhoneNumber'];
                                        $job_title = $user['JobTitle'];
                                        $user_group = $user['UserType'];
                                        $date_created = $user['CreatedDate'];
                                        $creator = $user['CreatedBy'];
                                        $actual_name = UserDetails($creator);
                                        if (empty($actual_name)) {
                                            $act_creator = "System";
                                        } else {
                                            $act_creator = $actual_name[0]['Username'];
                                        }
                                        ?>
                                        <tr id="<?php echo $user_id; ?>">

                                            <td> <?php echo $username; ?></td>
                                            <td><?php echo $job_title; ?></td>
                                            <td> <?php echo $email; ?></td>
                                            <td><?php echo $phone; ?></td>
											<td><?php echo $user_group; ?></td>
                                            <td><?php echo date('d M y', strtotime($date_created)); ?></td>
                                            <td><?php echo $act_creator; ?></td>
                                            <td>
                                        <a class="btn-floating btn-small waves-effect waves-light blue editProd" href="EditUser.php?UID=<?php echo $user_id; ?>" title="View and Edit User"><i class="small material-icons">mode_edit</i></a>
                                        <a class="btn-floating btn-small waves-effect waves-light green BtnReset" href="" title="Reset passkey"><i class="small material-icons">vpn_key</i></a>
                                 
                                            
                                            </td>

                                        </tr>    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>

    <div id="modal1" class="modal">
        <div class="modal-content">
            <h5>New User</h5>

            <div class="row">
                <form class="CreateUser col s12" method="post" >
                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter First Name" id="FirstName" name="FirstName" type="text" class="validate">
                            <label for="CoName">First Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter Last Name" id="LasName" name="LasName" type="text" class="validate">
                            <label for="LasName">Last Name</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter Username" id="Username" name="Username" type="text" class="validate">
                            <label for="Username">Username</label>
                        </div>
                        
                        
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter Job Title" id="JobTitle" name="JobTitle" type="text" class="validate">
                            <label for="JobTitle">Job Title</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter Email" id="email" name="email" type="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Enter Phone" id="phone" name="phone" type="text" class="validate">
                            <label for="phone">Phone Number</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s8">
                            <select id="usergroup" name="usergroup" placeholder="Choose user group" tabindex="-1" >

                                <option value="Administrator">Administrator</option>
                                <option value="Marshal">Marshal</option>

                            </select>
                            <label>Select User group</label>
                        </div>
                    </div>

                    <div class="row servicediv ">
                        <div class="input-field col s8">
                            
                            <select class="form-control" id="mega-s" name="servicecategories[]" size="7"  multiple>
                                <option value="All">All Services</option>
								<?php 
								$AllStocks = show_all_stocks();
								foreach($AllStocks as $Stk){
									$Code = $Stk["Code"];
									$Desc = $Stk["Description"];
									?>
                                <option value="<?php echo $Code; ?>"><?php echo $Desc; ?></option>
								<?php } ?>
                                
                            </select>
                            <label for="mega-skills">Can Sale</label>
                        </div>
                    </div>

                    <div class="row servicediv ">
                        <div class="input-field col s6">
                            <input placeholder="Enter Machine Number" min="0" max="999""  id="mcno" name="mcno" type="number" class="validate">
                            <label for="phone">Machine Number</label>
                        </div>

                        <div class="input-field col s6">
                            <input placeholder="Enter Operator Code" maxlength="2"  id="opcode" name="opcode" type="text" class="validate">
                            <label for="phone">Operator Code</label>
                        </div>

                    </div>


                </form>
            </div>


        </div>
        <div class="modal-footer">
            <div class ="row">

                <div class="col s4 l4 m4">
                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                    <a type="submit" name="btnUserDetails" class= "btnUserDetails waves-effect waves-light btn blue m-b-xs">Create User</a>
                </div>
                <div class="col s4 l4 m4">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn ">Cancel</a>
                </div>

            </div>

        </div>
    </div>
    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>

    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>
    <script>
        $(document).ready(function () {
            
            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);

            $(".servicediv").hide();
            $("#usergroup").on("change",function(){
                
                var usergroup = $(this).val();
                
                if(usergroup === "Marshal"){
                 $(".servicediv").show("slow");
                }
                else{
                  $(".servicediv").hide("slow");   
            }
            });
            
            $(".btnUserDetails").prop("disabled", true);

            $(".btnUserDetails").click(function (ev) {
                ev.preventDefault();
                $.post("engines/CreateUser.php", $(".CreateUser").serialize(),
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });
            
            $(".BtnReset").click(function(ev){
             ev.preventDefault();
           var userID = $(this).closest('tr').attr('id');
          $.get("engines/ResetPassword.php?userid="+userID,function(response){
              alert(response);
              location.reload(true);
          });
            
            });

        });
    </script>
</body>
</html>