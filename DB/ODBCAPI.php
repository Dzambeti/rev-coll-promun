<?php

/* Login functions first */
//@session_start();/
/* * *********************This is the main database API********It handles all database connection functions ****** */

// Mutare ODBC

//$dbodbc = new PDO('odbc:rec', 'Sysprogress', 'Zaq12wsx321');
//$dbodbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


  $dbodbc = new PDO('odbc:rec', 'Sysprogress', 'Sysprogress');
  $dbodbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 

$dbodbcmun  = new PDO('odbc:inc', 'Sysprogress', 'Sysprogress');
$dbodbcmun->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

Function GetItemCodes(){
     global $dbodbc;
    //$result=array();
    try {
        $sql = $dbodbc->prepare('select alloc, CODE, "desc-eng", vat from PUB.municf where alloc !=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function getCustomersFromPromun()
{
      global $dbodbcmun;
    //$result=array();
    try {
        $sql = $dbodbcmun->prepare('select acc,  addr1, addr2, addr3, balance,"last-pay-amt", "last-pay-dte", name from PUB.muncmf where company = 0 and balance > 0 and active <> 99');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}



    

function CreateMunrctctr($mcno, $cash, $recstatus, $recdate) {
    global $dbodbc;
    try {
        $valid = 0;
        $sql = $dbodbc->prepare('insert into PUB.munrctctr(mcno,cash,valid,"rec-status","rec-date") values (?,?,?,?,?)');
        $sql->execute(array($mcno, $cash, $valid, $recstatus, $recdate));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}
        //  CreateMunrct($code, $acc, $amount, $mc_no, $finalRecDate , $recno, $op_code, $ref, $paytype, $seqNo, $recstatus, $recname);
function CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname) {
    global $dbodbc;
    try {

        $sql = $dbodbc->prepare('insert into PUB.munrct(code,acc,amount,mcno,"rec-date",recno,opcode,ref,paytype,"seq-no","rec-status","rec-name") values (?,?,?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

// $code = 'PF';
// $acc = 16007407;
// $amount = 27.00;
// $mcno = '60';
// $recdate = date("Y-m-d",strtotime("2019-12-13"));
// $recno = 1;
// $opcode = '60';
// $ref = 0;
// $paytype = "B";
// $seqno = 1;
// $recstatus = 'U';
// $recname = 'I30070040143';

// $code = 'ZZ';
// $acc = 100061900;
// $amount = 10.0000;
// $mcno = 1;
// $recdate = date("Y-m-d",strtotime("2019-12-13"));
// $recno = 0;
// $opcode = 1;
// $ref = 0;
// $paytype = 'B';
// $seqno = 1;
// $recstatus = 'U';
// $recname = "";
// print_r(CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname));


function getMeterData($acc){
    global $dbodbcmun;
    
    try {
        $ew = "W";
        $meterno = "1";
        $sql = $dbodbcmun->prepare('select "meter-no",acc,route from PUB.munrmf where company = 0 and "meter-no" != ? and ew=? and acc=?');
        $sql->execute(array($meterno,$ew,$acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}



function getMeterInfo(){
    global $dbodbcmun;
    
    try {
        $ew = "W";
        $sql = $dbodbcmun->prepare('select route,acc,"meter-no", read, "read-date" from PUB.munrmf where company = 0 and ew=? and type <> 3');
        $sql->execute(array($ew));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
     return $result;
}


function getMeterAccount($acc)
{
    global $dbodbcmun;
    try {
        $sql = $dbodbcmun->prepare('select muncmf.addr1, muncmf.addr2, muncmf.addr3, muncmf.suburb, muncmf.ward,muncmf.name from PUB.muncmf where company = 0 and muncmf.acc = ? and active <> 99');
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function getRouteName($RouteNo)
{
    global $dbodbcmun;
    try {
        $sql = $dbodbcmun->prepare('select munrou."route-desc" from PUB.munrou where munrou."route-no" = ?');
        $sql->execute(array($RouteNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}


function getNotesInfo(){
    global $dbodbcmun;
    
    try {
        
        $sql = $dbodbcmun->prepare('select distinct(code) as code,descr  from PUB.munntd ');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
     return $result;
}

function getRoutes(){
     global $dbodbcmun;
    try {
        $sql = $dbodbcmun->prepare('select "route-id","route-no","route-desc","number-of-meters","last-read-date","next-read-date" from PUB.munrou');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result; 
}





