<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
    $PaymentID = $_GET["Pid"];
   $PaymentDet = GetPaymentWithId($PaymentID);
    $CarReg = $PaymentDet[0]["CustomerName"];
    $AmntPaid = base64_decode($_GET["Val"]);
    $CarDet = GetOutstandingSales($CarReg);
$totalsArray = array();
    foreach ($CarDet as $Data) {
       
        $Bal = $Data["InvoiceTotal"];
        array_push($totalsArray, $Bal);
    }
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>

            <!-- Title -->
            <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
            <meta charset="UTF-8">
            <meta name="description" content="Responsive Admin Dashboard Template" />
            <meta name="keywords" content="admin,dashboard" />
            <meta name="author" content="Steelcoders" />

            <!-- Styles -->
            <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
            <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">


            <!-- Theme Styles -->
            <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
            <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

        </head>
        <body>
            <?php require 'config.php'; ?>

            <main class="mn-inner">

                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s6 m6 l6">
                                        <span class="card-title">Car Receipts Summary</span><br>
                                        <span class="">Total Balance: $<?php echo array_sum($totalsArray); ?></span><br>
                                        <span class="">Total Paid: $<?php echo round($AmntPaid,2); ?></span>
                                    </div>
                                     <div class="col s6 m6 l6 right-align">
                                    <a class="waves-effect waves-light btn red m-b-xs ClearPayments">Update Payments</a>
                                </div>
                                </div>
                                
                               

                                <br>
                                <table id="example" class="display responsive-table datatable-example">
                                    <thead>
                                        <tr>
                                            <th>Receipt Number</th>
                                            <th>Area</th>
                                            <th>Balance</th>
                                            <th>Date</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        foreach ($CarDet as $Data) {
                                            $RecNum = $Data["InvoiceNum"];
                                            $Area = $Data["Area"];
                                            $Date = date("D m y", strtotime($Data["TimeIn"]));
                                            $Bal = $Data["InvoiceTotal"];
                                            ?>
                                            <tr id="<?php echo $RecNum; ?>">
                                                <td><?php echo $RecNum; ?> </td>
                                                <td><?php echo $Area; ?></td>
                                                <td> <?php echo $Bal; ?></td>
                                                <td> <?php echo $Date; ?> </td>
                                            </tr> 

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

        </div>
        <div class="left-sidebar-hover"></div>

     
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>

        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        <script src="assets/js/pages/table-data.js"></script>

        <script>
            $(document).ready(function () {
              var name = '<?php echo $Username; ?>';
                setTimeout(function () {
                Materialize.toast('Welcome to ' + name + '!', 4000)
            }, 4000);

           

          
            var CarReg;
            $(".ClearPayments").click(function (ev) {
                ev.preventDefault();
                var amount = <?php echo $AmntPaid; ?>;
                var pid = <?php echo $PaymentID; ?>;
                $.get("engines/GetCarReceipts.php?id="+pid+"&amount="+amount, 
                function (feedback) {
        console.log(feedback);        
         var resp = $.parseJSON(feedback); 
           alert(resp.msg); 
         if(resp.status === "ok"){
                window.location.href = "ManagePayments";
              }
              
                  
                });

            });

        });
    </script>
</body>
</html>