<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
    
    $CoInfo = this_co_details();
    $CoName = $CoInfo[0]["CompanyName"];
    $CoVat = $CoInfo[0]["CompanyVATN"];
    $CoBpn = $CoInfo[0]["CompanyBPN"];
    $CoMail = $CoInfo[0]["CompanyEmail"];
    $CoAddress = $CoInfo[0]["CompanyAddress"];
    $McNo = $CoInfo[0]["McNo"];
    $OpCode = $CoInfo[0]["OpCode"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis || <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title"><?php echo $CoName; ?> Details</div>
                    </div>
                    <div class="col s12 m12 l6">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title"></span><br>
                                <div class="row">
                                    <form class="CreateCo col s12" method="post" >
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="Enter Company Name" id="CoName" name="CoName" value="<?php echo $CoName; ?>" type="text" class="validate">
                                                <label for="CoName">Company Name</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input placeholder="Enter email" id="email" name="email" type="email" value="<?php echo $CoMail; ?>" class="validate">
                                                <label for="Vat">Company Email</label>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="Enter BPN" id="BPN" name="BPN" type="text" value="<?php echo $CoBpn; ?>" class="validate">
                                                <label for="BPN">Company BPN</label>
                                            </div>
                                             
                                             <div class="input-field col s6">
                                                <input placeholder="Enter VAT" id="Vat" name="Vat" type="text" value="<?php echo $CoVat; ?>" class="validate">
                                                <label for="Vat">Company VAT</label>
                                            </div>
                                          
                                        </div>
                                         <div class="row">
                                            <div class="input-field col s12">
                                                <input placeholder="Enter address, using comma to split address componets" value="<?php echo $CoAddress; ?>" id="Address" name="Address" type="text" class="validate">
                                                <label for="Address">Company Address</label>
                                            </div>
                                           
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="Enter OpCode" id="BPN" name="OpCode" maxlength="3" type="text" value="<?php echo $OpCode; ?>" class="validate">
                                                <label for="BPN">Online OP Code</label>
                                            </div>
                                             
                                             <div class="input-field col s6">
                                                 <input placeholder="Enter Online MCNo" max="999" id="Vat" name="McNO" type="number" value="<?php echo $McNo; ?>" class="validate">
                                                <label for="Vat">Online Machine No</label>
                                            </div>
                                          
                                        </div>
                                        
                                        <div class ="row">
                                        
                                         <div class="col-sm-6">
                                                  <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                                  <a type="submit" name="loginbutton" class= "btnUpdateCO waves-effect waves-light btn teal">Update Details</a>
                                                   </div>
                                        </div>
                                        
                                         <br>    <div class="ajax-loaders saving-spinner slider"> <b>...Updating <?php echo $CoName; ?> Details...</b></div>  
                                    <div class="form-group resp col-md-6"></div>  
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                      
                       
                      
                    </div>
                
                </div>
            </main>

    </div>
    <div class="left-sidebar-hover"></div>


    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>

    <script>
        $(document).ready(function () {
 $(".resp").hide();
   $('.saving-spinner').hide('slow');
                $('.btnUpdateCO').click(function (ev)
                {
                    ev.preventDefault();
                    $('.btnUpdateCO').prop('disabled', true);
                    $('.saving-spinner').slideDown('slow');
                    $.post('engines/updateCoData.php', $(".CreateCo").serialize(),
                            function (response) {
                                console.log(response);
                                var resp = $.parseJSON(response);

                                $('.saving-spinner').slideUp('slow');
                               $(".resp").html(resp.msg);
                                $(".resp").slideDown('slow');
                             
                                 
                                $('.btnUpdateCO').prop('disabled', false);
                            });
                    return false;
                });

        });
    </script>
</body>
</html>