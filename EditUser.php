<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
    $UserID = $_GET["UID"];
    $UserInfo = UserDetails($UserID);
    $user_rec_info = get_user_rec_control($UserID);

    if(empty($user_rec_info)){
        $mc_no = NULL;
        $op_code = NULL;
            }
            else{
                $mc_no = $user_rec_info[0]["McNo"];
                $op_code =  $user_rec_info[0]["OpCode"]; 
            }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row">

                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Edit User</span>
                                </div>


                            </div>
  <div class="row">
                <form class="EditUser col s12" method="post" >
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter First Name" value="<?php echo $UserInfo[0]["UserFirstName"]; ?>" id="FirstName" name="FirstName" type="text" class="validate">
                            <label for="CoName">First Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Last Name" value="<?php echo $UserInfo[0]["UserSurname"]; ?>" id="LasName" name="LasName" type="text" class="validate">
                            <label for="LasName">Last Name</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Username" value="<?php echo $UserInfo[0]["Username"]; ?>" id="Username" name="Username" type="text" class="validate">
                            <label for="Username">Username</label>
                        </div>
                        
                        
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Job Title" value="<?php echo $UserInfo[0]["JobTitle"]; ?>" id="JobTitle" name="JobTitle" type="text" class="validate">
                            <label for="JobTitle">Job Title</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Email" value="<?php echo $UserInfo[0]["EmailAddress"]; ?>" id="email" name="email" type="email" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter Phone" value="<?php echo $UserInfo[0]["UserPhoneNumber"]; ?>" id="phone" name="phone" type="text" class="validate">
                            <label for="phone">Phone Number</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <select id="usergroup" name="usergroup" placeholder="Choose user group" tabindex="-1" >
                                <option value="">Choose User Group</option>
                                <option value="Administrator" >Administrator</option>
                                <option value="Marshal">Marshal</option>

                            </select>
                            <label>Select User group</label>
                        </div>
                    </div>

                    <div class="row servicediv ">
                        <div class="input-field col s6">
                            
                            <select class="form-control" id="mega-s" name="servicecategories[]" size="7"  multiple>
                                 <option value="">Choose Services</option>
                                <option value="All">All Services</option>
								<?php 
								$AllStocks = show_all_stocks();
								foreach($AllStocks as $Stk){
									$Code = $Stk["Code"];
									$Desc = $Stk["Description"];
									?>
                                <option value="<?php echo $Code; ?>"><?php echo $Desc; ?></option>
								<?php } ?>
                                
                            </select>
                            <label for="mega-s">Can Receipt?</label>
                        </div>
                                </div>

                                
                    
                    <div class="row servicediv">
                        <div class="input-field col s6">
                            <input placeholder="" value="<?php echo $UserInfo[0]["MarshalServices"]; ?>" readonly="true" id="activeserv" name="activeserv" type="text" class="validate">
                            <label for="activeserv">Active Services</label>
                        </div>
                    </div>

                    <div class="row servicediv ">
                        <div class="input-field col s6">
                            <input placeholder="Enter Machine Number" min="0" max="999" value="<?php echo $mc_no; ?>" id="mcno" name="mcno" type="number" class="validate">
                            <label for="phone">Machine Number</label>
                        </div>

                        <div class="input-field col s6">
                            <input placeholder="Enter Operator Code" maxlength="2" value="<?php echo $op_code; ?>" id="opcode" name="opcode" type="text" class="validate">
                            <label for="phone">Operator Code</label>
                        </div>

                    </div>
                    
                     <div class="col s4 l4 m4">
                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                    <a type="submit" name="btnUserDetails" class= "btnUserDetails waves-effect waves-light btn blue m-b-xs">Update User</a>
                </div>


                </form>
            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>

   
       
    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>

    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>
    <script>
        $(document).ready(function () {
            
            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);

            $(".servicediv").hide();
            $("#usergroup").on("change",function(){
                
                var usergroup = $(this).val();
                
                if(usergroup === "Marshal"){
                 $(".servicediv").show("slow");
                }
                else{
                  $(".servicediv").hide("slow");   
            }
            });
            
            $(".btnUserDetails").prop("disabled", true);

            $(".btnUserDetails").click(function (ev) {
                ev.preventDefault();
                var id = '<?php echo $UserID; ?>';
                $.post("engines/EditUser.php?UID="+id, $(".EditUser").serialize(),
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });

        });
    </script>
</body>
</html>