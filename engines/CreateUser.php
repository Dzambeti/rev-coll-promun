<?php
require '../DB/DBAPI.php';


    $userfirstmame = $_POST['FirstName'];
    $usersurname = $_POST['LasName'];
    $username = $_POST['Username'];
    $email = $_POST['email'];
    $jobtitle = $_POST['JobTitle'];
    $phone_number  = $_POST['phone'];
    $user_group = $_POST["usergroup"];
	$AllServices = "All";
	
    if($user_group == "Marshal")
    {
    @$Services = $_POST["servicecategories"];
    @$mc_no = $_POST["mcno"];
    @$op_code = $_POST["opcode"];
    

    if(empty($Services)){
        $rslt["msg"] ='Services can not be left out for user type Masrshal!'; 
        $rslt["status"] = "error";
        echo json_encode($rslt);
        die();
    }
    else if($mc_no == NULL or $op_code == NULL){
        $rslt["msg"] ='Machine number or operator code can not be null for user of type Marshall.!'; 
        $rslt["status"] = "error";
        echo json_encode($rslt);
        die();
    }
    else if($mc_no > 999){
        $rslt["msg"] ='Machine number can not be greater 999.!'; 
        $rslt["status"] = "error";
        echo json_encode($rslt);
        die();
    }
       
		if(in_array($AllServices, $Services))
		{
			$AllCodes = array();
				foreach(ShowAllProdCodes() as $Codes){
					$Cod = $Codes["Codes"];
					array_push($AllCodes,$Cod);
				}
				$MarshalServices = implode($AllCodes,",");
				
		}
		else{
			$MarshalServices = implode($Services,",");
		}
		
    }
    else{
        $MarshalServices = "";
    }

$status = 0;
$created_by = $_SESSION['acc'];


$password = md5($username);
//check email, phone number and  username
$usernames = array();
$emails = array();
$phones = array();

  $all_users = get_all_users();
  foreach($all_users as $user){
      $DB_username = $user['Username'];
      $db_email = $user['EmailAddress'];
      $db_phoneNUmber =  $user['UserPhoneNumber'];
      array_push($phones, $db_phoneNUmber);
       array_push($emails, $db_email);
        array_push($usernames, $DB_username);
      
  }
if(empty($username) || empty($user_group))
{
    $rslt["msg"] ='Username or user group can not be empty!'; 
      $rslt["status"] = "error";
}

elseif(in_array($phone_number, $phones)){
      $rslt["msg"] = 'Phone number already in use by another user. Verify this phone number please.'; 
      $rslt["status"] = "error";
}
elseif(in_array($email, $emails)){
      $rslt["msg"] = ' Email Address already in use. Enter correct email address'; 
      $rslt["status"] = "error";
}
elseif(in_array($username, $usernames)){
      $rslt["msg"] = ' Username in use. Please choose another username.'; 
      $rslt["status"] = "error";
}
else{
                       
$new_user = Create_User($username, $password,$userfirstmame,$usersurname,$jobtitle, $email,$user_group,$phone_number,$MarshalServices, $status,$created_by);
if($new_user['status']=="ok"){
$UserID = $new_user['id'];
    if($user_group == "Marshal"){
        $update_rec = create_rec_control($UserID,$mc_no,$op_code);
    }

    $rslt["msg"] = 'User successfully added to the system! Please give the user the access details. Username: '.$username.' and default password: '.$username;
$rslt["status"] = "ok";
    
}
else{
    $rslt["status"] = "error";
     $rslt["msg"] = ' Failed to create user account. ERROR: '.$new_user['status']; 
}
}

echo json_encode($rslt);







