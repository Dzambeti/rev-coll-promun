<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];

    $shift = $_GET["Shft"];
    if ($shift == "_all") {
        $GetRec = GetReceipts();
        $Owings = GetOwings();
         $paid = GetPaid();
    }
    else if($shift == "_out"){
         $GetRec = GetOwings();
         $Owings = GetOwings();
          $paid = GetPaid();
    }  else if($shift == "_in"){
         $GetRec = GetPaid();
         $Owings = GetOwings();
         $paid = GetPaid();
    }
    else {
        $GetRec = GetShiftReceipts($shift);
        $Owings = GetShiftOwingReceipts($shift);
        $paid = GetShiftPaid($shift);
    }
    
    $totals = array();
    $OwingsArray = array();
    foreach ($GetRec as $Rc) {
        $InvTot = $Rc["InvoiceTotal"];
        array_push($totals, $InvTot);
    }
    
    foreach($Owings as $owing){
        $InvTot = $Rc["InvoiceTotal"];
        array_push($OwingsArray, $InvTot);
    }
    
    $Paid = array_sum($totals) - array_sum($OwingsArray);
    $NumRecPaid = sizeof($paid);
    
    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/js2/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="assets/js2/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="assets/js2/datatables/buttons.dataTables.min.css">
         <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            
             <div class="row FocusDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>Cash Up Summary for shift <?php echo $shift; ?></h5>


                             <table id = "example" class = "display responsive-table datable striped tblCashUp">
                                <thead>
                                    <tr>
                                        <th>Shift Number</th>
                                        <th>Currency</th>
                                        <th>Amount</th>
                                                                            </tr>
                                </thead>

                                <tbody>
                                    <?php
                                     if($shift == "_in")
                                { 
                                         $shift = "All Paid";
                                          $CashUp = cashUpAllTime();
                                } else{
                                   $CashUp = getCashUpSummary($shift);
                                }
                                $SummaryTot = 0;
                                    foreach ($CashUp as $Rec) {
                                        $Total = $Rec["tot"];
                                        $Currency = $Rec["Currency"];
                                        $SummaryTot +=$Total;
                                      
                                        ?>
                                        <tr>
                                            <td><?php echo $shift; ?> </td>
                                            <td> <?php echo $Currency; ?></td>
                                            <td> <?php echo number_format($Total,2); ?></td>
                                           
                                            


                                        </tr>  
                                        
                                    <?php } ?>
                                        
                                        <tr>
                                            <td><b>Total Summary: </b></td>
                                            <td> ----- </td>
                                            <td><?php echo  number_format($SummaryTot,2); ?> </td>
                                        </tr>
                                </tbody>
                                
                            </table>


                        </div>
                    </div>
                </div>

            </div>

            
            <div class="row">

                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <?php if ($shift == "_all") { ?>
                                       
                                        <span class="card-title">Tickets Summary - All Shifts</span> <br>
                                          <span>Total Tickets : <b class="green-text">$<?php echo array_sum($totals)." for ".count($GetRec); ?></b></span>
                                          <br><span>Total Paid : <b class="blue-text">$<?php echo $Paid." for $NumRecPaid"; ?></b></span> 
                                          <br> <span>Total Outstanding : <b class="red-text">$<?php echo array_sum($OwingsArray)." for ".count($Owings).""; ?></b></span> 
                              
                                       
                                    <?php }elseif($shift == "_out"){?>
                                        <span class="card-title">Tickets Summary - Outstanding</span> <br>
                                   <?php }elseif($shift == "_in"){?>
                                        <span class="card-title">Tickets Summary - Paid</span> <br>
                                   <?php } else  { ?>
                                         <span class="card-title">Tickets Summary for shift: <?php echo $shift; ?></span><br>
                                           <span>Total Tickets : <b class="green-text">$<?php echo array_sum($totals)." for ".count($GetRec); ?></b></span>
                                          <br><span>Total Paid : <b class="blue-text">$<?php echo $Paid." for $NumRecPaid"; ?></b></span> 
                                          <br> <span>Total Outstanding : <b class="red-text">$<?php echo array_sum($OwingsArray)." for ".count($Owings).""; ?></b></span> 
                              
                                        
                                    <?php } ?>
                                        
                                        
                                         
                                </div>
                               
                                 <div class="col s6 m6 l6 right-align">
                                    <a class="btnFocusDiv waves-effect waves-light btn red m-b-xs">Cash Up Summary</a>
                                </div>
                               



                            </div>
                            
                             <div class="row">
                                <div class="col s6 m6 l6">
                                  
                                       
                                     
                                </div>
                                 
                                 
                            </div>
                            
                             <?php if($UnusedPayments > 0){ ?>
                             
                                <div class="col s11 m11 l11">
                                    <span class="red-text"><b>There are payments not yet updated. Click on payments tab on menu to update payments!</b></span>
                           
                                </div><br>
                        
                             <?php } ?>

                         
                           

                            <br>
                            <table id = "example" class = "display responsive-table datable datatable-example">
                                <thead>
                                    <tr>
                                        <th>Ticket #</th>
                                        <th>Shift #</th>
                                        <th>Reference</th>
                                        <th>Marshal</th>
                                        <th>Amount ($)</th>
                                        <th>Status</th>
                                        <th>Date Done</th>
                                        <th class="hidden-print">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    foreach ($GetRec as $Rec) {
                                        $RecNum = $Rec["InvoiceNum"];
                                        $ShftNum = $Rec["ShiftRefence"];
                                         $CarReg = $Rec["CustomerName"];
                                        $Marshal = $Rec["SalesManName"];
                                        $Amount = $Rec["InvoiceTotal"];
                                        $Status = $Rec["InvoiceStatus"];
                                        if($Status=="Paid"){
                                             $Status = '<div class="chip green">' . $Status . '</div>';
                                        }
                                        else{
                                             $Status = '<div class="chip red">' . $Status . '</div>';
                                        }
                                        $DateCre = date("d M y", strtotime($Rec["CreatedDate"]));
                                        ?>
                                        <tr>
                                            <td><?php echo $RecNum; ?> </td>
                                            <td> <?php echo $ShftNum; ?></td>
                                              <td> <?php echo $CarReg; ?></td>
                                            <td><?php echo $Marshal; ?></td>
                                            <td><?php echo $Amount; ?></td>
                                            <td><?php echo $Status; ?></td>
                                            <td><?php echo $DateCre; ?></td>
                                            <td class="hidden-print">
                                                <a class="btn-floating btn-small waves-effect waves-light blue " href="Ticket.php?TktNum=<?php echo $RecNum; ?>" title="View Ticket"><i class="small material-icons">visibility</i></a>
                                                <a class="btn-floating waves-effect waves-light red" href="InvMap.php?TktNum=<?php echo $RecNum; ?>" title="View Receipt Location"><i class="small material-icons">my_location</i></a>
                                            </td>


                                        </tr>    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>

  
    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js2/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js2/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/js2/datatables/buttons.flash.min.js"></script>
    <script src="assets/js2/datatables/jszip.min.js"></script>
    <script src="assets/js2/datatables/pdfmake.min.js"></script>
    <script src="assets/js2/datatables/vfs_fonts.js"></script>
    <script src="assets/js2/datatables/buttons.html5.min.js"></script>
    <script src="assets/js2/datatables/buttons.print.min.js"></script>
    <script>
        $(document).ready(function () {
            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);


jQuery('.datatable-example').dataTable({
    language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        },
            columnDefs: [{orderable: false, targets: [4]}],
            pageLength: 20,
            "order": [[2, "asc"]],
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            dom: 'Bfrtip',
           
             buttons: [
            {
                extend: 'excelHtml5',
                title: '<?php echo "Shift_".$shift;?>'
            },
            {
                extend: 'pdfHtml5',
                title: '<?php echo "Shift_".$shift;?>'
            }]
        });
        
                    $(".FocusDiv").hide();
        
         $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });
            
            
            jQuery('.tblCashUp').dataTable({
    language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>' 
        }
        },
            columnDefs: [{orderable: false, targets: [1]}],
            pageLength: 20,
            "order": [[2, "asc"]],
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            dom: 'Bfrtip',
           
             buttons: [
            {
                extend: 'excelHtml5',
                title: '<?php echo "cashUp_".$shift;?>'
            },
            {
                extend: 'pdfHtml5',
                title: '<?php echo "Shift_".$shift;?>'
            }]
        });
        

        });
    </script>
</body>
</html>