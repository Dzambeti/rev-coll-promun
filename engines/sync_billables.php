<?php

require '../DB/DBAPI.php';
require '../DB/ODBCAPI.php';

/*
* 1. check if there are transactions
* 2. get pair values for date and cashier
* 3. write function to get all trans given date and cashier
* 4. insert each set and create its munrctctr
* number of pairs define number of control files
*/

$transactions = show_distnc_collections_mc_combinations();  
// 1. check if there are transactions
if(empty($transactions)){
    $rslt["status"] = "fail";
    $rslt["msg"]  = "No transactions to sync!";
    echo json_encode($rslt);
    die();
}
else{
   
    foreach($transactions as $tran){
        $grouped_date = $tran["dates"];
        $sales_rep = $tran["SalesRep"];

        $salesman_header = show_collect_mac_details($sales_rep); 
        if(empty($salesman_header)){
            $mc_no = NULL;
            $op_code = NULL;
            continue;
        }
        else{
            $mc_no = $salesman_header[0]["McNo"];
            $op_code = trim($salesman_header[0]["OpCode"]);
      

        $combination_transactions = get_pair_value_combinations_for_recdate_salesman($grouped_date,$sales_rep);
        //  print_r($combination_transactions);
        //  die();
        $i = 0;
        foreach($combination_transactions as $data){
                 $code = 'ZZ';
                $acc_name_combination = $data["CustomerName"];
                $arr = explode("#", $acc_name_combination, 2);
                           $acc = (float)$arr[0];

                $AccDet = GetAccountData($acc);
                // print_r($AccDet);
                @$customer_name = $AccDet[0]["CustomerName"];
                @$invNumber = $data["ReferenceNumber"];
                $tender_info = getTenderCurrency($invNumber);
                $paytype = trim($tender_info[0]["SourceReference"]);
                $ref = 0;
                $amount = round($data["Amount"],2);
                $recno = $i; 
                $recname = "";
                $seqNo = 1;
                $recstatus = 'U';
                $finalRecDate = date("Y-m-d",strtotime($grouped_date));
                $paymentID = $data["PaymentID"];
                // die("opcode=$op_code,");
          
                $CreateRec = CreateMunrct($code, $acc, $amount, $mc_no, $finalRecDate , $recno, $op_code, $ref, $paytype, $seqNo, $recstatus, $recname);
                if($CreateRec["status"] == "ok")
                {
                    $i+=1;
                    $status = 'munrct-ok';
                    update_billables_rec_status($status,  $paymentID);
                }
                else{
                    $rslt["status"] = "fail";
                    $rslt["msg"]  = "Failed to create transactions number $i and payment ID : $paymentID - {following details:Date: $finalRecDate, code = $code, acc = $acc, amount = $amount,
                    mc-no = $mc_no, op-code=$op_code, ref = $ref, paytype = $paytype, seq-no = $seqNo, rec-status = $recstatus, rec-name = $recname} : Error - ".$CreateRec["status"];
                    echo json_encode($rslt);
                    die();
                }
            }
            //create munrctctr
        if($CreateRec["status"] == "ok")
        {
            $cash = 99999.00;
            $recstatus = "U";        
       $CreatHdr =  CreateMunrctctr($mc_no, $cash, $recstatus, $finalRecDate);
       if($CreatHdr["status"] == "ok")
        {
            $rslt["status"] = "ok";
            $rslt["msg"] = "Receipts created successfully";
        }
        else{
            $rslt["status"] = "fail";
            $rslt["msg"]  = "Failed to create transactions header for machine - $mc_no and operator $op_code. : Error - ".$CreatHdr["status"];
            echo json_encode($rslt);
            die();  
        }
        }
        else{
            $rslt["status"] = "fail";
            $rslt["msg"]  = "Failed to create transactions for machine - $mc_no and operator $op_code. : Error - ".$CreateRec["status"];
            echo json_encode($rslt);
            die();  
        }
    }
    }
}

echo json_encode($rslt);
